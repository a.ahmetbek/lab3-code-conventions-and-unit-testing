package com.db.controllers;

import java.util.Collections;
import java.util.Date;

import com.hw.db.DAO.ThreadDAO;

import com.hw.db.DAO.UserDAO;
import com.hw.db.controllers.threadController;
import com.hw.db.models.Message;
import com.hw.db.models.User;
import com.hw.db.models.Vote;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.hw.db.models.Thread;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.Timestamp;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;


class threadControllerTests {
    private Thread thread;
    private final String stub = "slug";

    @BeforeEach
    @DisplayName("Initialize thread controller tests")
    void createThreadTest() {
        thread = new Thread("Ali", new Timestamp(new Date().getTime()), "forum", "msg", stub, "title", 1);
    }

    @Test
    @DisplayName("Check id or slug test")
    void CheckIdOrSlugTest() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadById(Integer.parseInt("0")))
                    .thenReturn(thread);
            threadMock.when(() -> ThreadDAO.getThreadBySlug(stub))
                    .thenReturn(thread);

            threadController controller = new threadController();
            assertEquals(thread, controller.CheckIdOrSlug("0"), "Found by id");
            assertNull(controller.CheckIdOrSlug("777"), "Not found by id");

            assertEquals(thread, controller.CheckIdOrSlug(stub), "Found by slug");
            assertNull(controller.CheckIdOrSlug("stub"), "Not found by slug");
        }
    }

    @Test
    @DisplayName("Correct post creation test")
    void correctlyCreatesPost() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug(stub))
                    .thenReturn(thread);

            threadController controller = new threadController();

            assertEquals(ResponseEntity.status(HttpStatus.CREATED).body(Collections.emptyList()),
                    controller.createPost(stub, Collections.emptyList()), "Created empty post");

            assertEquals(thread, ThreadDAO.getThreadBySlug(stub), "Found by slug");
        }
    }

    @Test
    @DisplayName("Get posts")
    void PostsTest() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug(stub))
                    .thenReturn(thread);

            threadController controller = new threadController();

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(Collections.emptyList()),
                    controller.Posts(stub, 1, 1, "flat", false), "Post found");
        }
    }

    @Test
    @DisplayName("Change post (404)")
    void ChangeTest() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug(stub)).thenReturn(null);
            threadMock.when(() -> ThreadDAO.change(null, thread)).thenThrow(new DataAccessException("Error"){});

            threadController controller = new threadController();

            assertEquals(ResponseEntity.status(HttpStatus.NOT_FOUND)
                            .body(new Message("Раздел не найден.")).getStatusCode(),
                    controller.change(stub, thread).getStatusCode(), "Post is not found");
        }
    }

    @Test
    @DisplayName("Info post")
    void InfoTest() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug(stub)).thenReturn(thread);

            threadController controller = new threadController();

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread),
                    controller.info(stub), "Info received");
        }
    }

    @Test
    @DisplayName("Test createVote method")
    void createVoteTest() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug(stub)).thenReturn(thread);

            try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                User user = new User("some","some@email.mu", "name", "nothing");
                Vote vote = new Vote("some", 1);
                userMock.when(() -> UserDAO.Info("some")).thenReturn(user);

                threadController controller = new threadController();
                assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread),
                        controller.createVote(stub, vote), "Vote created");
            }
        }
    }
}
